<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        echo "Helloworld";
        echo "<br>";

        // Komentar pertama

        # Komentar kedua

        /*
            Komentar ketiga
            Komentar keempat
        */

        $x = 5;
        $y = 10;
        $z = $x + $y;
        echo $z;

        echo "<br>";

        $text="Pemrograman Web";
        echo "Saya suka " . $text;
    ?>
</body>
</html>